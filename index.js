var CSGO = require('./csgo'),
	express = require('express'),
	nunjucks = require('nunjucks');

var CSGORanks = function CSGORanks(args) {	
	csgo = new CSGO.CSGOClient(args.accountName, args.password, args.sentryFileName);

	app = express();

	nunjucks.configure('templates', {
		autoescape: true,
		express: app
	});

	app.get('/', function(req, res) {
		res.render('index.html');
	});

	app.get('/ranks', function(req, res) {
		var accountIds = [];
		if (req.query.steamid)
			accountIds = [csgo.toAccountID(req.query.steamid)];
		csgo.getRanks(accountIds, function(profileData) {
			if (req.xhr || req.query.raw)
				res.send(profileData);
			else
				res.render('ranks.html', profileData);
		});
	});

	app.listen(args.httpPort);
};

module.exports.CSGORanks = CSGORanks;