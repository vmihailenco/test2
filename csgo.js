/*
	CS:GO MM rank retriever. Includes cooldown info.
*/

var EventEmitter = require('events').EventEmitter,
	fs = require('fs'),
	util = require("util"),
	bignum = require("bignum"),
	Steam = require('steam'),
	Schema = require('protobuf').Schema,
	base_gcmessages = new Schema(fs.readFileSync(__dirname + "/generated/base_gcmessages.desc")),
	gcsdk_gcmessages = new Schema(fs.readFileSync(__dirname + "/generated/gcsdk_gcmessages.desc")),
	cstrike15_gcmessages = new Schema(fs.readFileSync(__dirname + "/generated/cstrike15_gcmessages.desc")),
	steammessages = new Schema(fs.readFileSync(__dirname + "/generated/steammessages.desc")),
	protoMask = 0x80000000,
	CSGO = exports;

CSGO.EMsg = {
	ClientWelcome: 4004,
	ClientHello: 4006,
	ClientRequestPlayersProfile: 9127,
	PlayersProfile: 9128,
};
CSGO.MMRanks = [
	"", "Silver I", "Silver II", "Silver III", "Silver IV", "Silver Elite",
	"Silver Elite Master", "Gold Nova I", "Gold Nova II", "Gold Nova III",
	"Gold Nova Master", "Master Guardian I", "Master Guardian II",
	"Master Guardian Elite", "Distinguished Master Guardian",
	"Legendary Eagle", "Legendary Eagle Master",
	"Supreme Master First Class", "The Global Elite",
];

var CSGOClient = function CSGOClient(accountName, password, sentryFileName) {
	this.steam = new Steam.SteamClient();
	this._appid = 730;
	this._gcClientHelloIntervalId = null;
	this.ready = false;

	var self = this;

	if (fs.existsSync('servers')) {
		Steam.servers = JSON.parse(fs.readFileSync('servers'));
	}

	var logonDetails = {
		accountName: accountName,
		password: password
	};
	if (sentryFileName || false) {
		logonDetails.shaSentryfile = require('crypto').createHash('sha1').update(fs.readFileSync(sentryFileName)).digest();
	}
	this.steam.logOn(logonDetails);

	this.steam.on('loggedOn', function() {
		self.steam.setPersonaState(Steam.EPersonaState.Online);
		self.steam.gamesPlayed([self._appid]);
		self._gcClientHelloIntervalId = setInterval(self._sendHello, 2500);
	});

	this.steam.on('fromGC', function(appid, type, message, callback) {
		callback = callback || null;
		var kMsg = type & ~protoMask;

		if (kMsg in self._handlers) {
			if (callback)
				self._handlers[kMsg].call(self, message, callback);
			else
				self._handlers[kMsg].call(self, message);
		} else {
			console.log('Unknown message: ' + kMsg);
		}
	});

	this._sendHello = function() {
		self.steam.toGC(self._appid, CSGO.EMsg.ClientHello | protoMask, gcsdk_gcmessages.CMsgClientHello.serialize({
			version: 0,
			socacheHaveVersions: []
		}));
	};
};
util.inherits(CSGOClient, EventEmitter);

CSGOClient.prototype.getRanks = function(accountIds, callback) {
	if (!this.ready) {
		return callback({success: false, error: "GC client is not ready"});
	}
	var requestId = Math.floor((Math.random() * 10000) + 1),
		eventName = 'players-' + requestId,
		message = cstrike15_gcmessages.CMsgGCCStrike15_v2_ClientRequestPlayersProfile.serialize({
			requestId: requestId,
			accountIds: accountIds,
		}),
		self = this;
	this.steam.toGC(this._appid, CSGO.EMsg.ClientRequestPlayersProfile | protoMask, message);
	var responseHandler = function(playersRes) {
		clearTimeout(responseTimeout);
		response = {success: true};
		if (!('accountProfiles' in playersRes)) {
			return callback(response);
		}
		var steamIdToPlayerIdx = {};
		response.accountProfiles = playersRes.accountProfiles.map(function(profile, idx) {
			steamIdToPlayerIdx[profile.steamId = self.toSteamID(profile.accountId)] = idx;
			if (profile.ranking)
				profile.ranking.rankName = CSGO.MMRanks[profile.ranking.rankId];
			return profile;
		});
		var steamIds = Object.keys(steamIdToPlayerIdx);
		message = Steam.Internal.CMsgClientRequestFriendData.serialize({
			personaStateRequested: 3154,
			friends: steamIds
		})
		var friendHandler = function(friend) {
			var steamIdsIdx = steamIds.indexOf(friend.friendid);
			if (steamIdsIdx == -1) // we don't care about that friend right now
				return;
			var accountProfilesId = steamIdToPlayerIdx[friend.friendid],
				avatarHash = friend.avatarHash.toString('hex');
			if (avatarHash)
				response.accountProfiles[accountProfilesId].avatarUrl = [
					"http://media.steampowered.com/steamcommunity/public/images/avatars/",
					avatarHash.substring(0, 2), "/", avatarHash, "_medium.jpg"].join("");
			response.accountProfiles[accountProfilesId].playerName = friend.playerName;
			steamIds.splice(steamIdsIdx, 1);
			if (!steamIds.length) {
				steam.removeListener('user', friendHandler);
				callback(response);
			}
		};
		self.steam.on('user', friendHandler);
		self.steam._send(Steam.EMsg.ClientRequestFriendData | protoMask, message);
	};
	this.once(eventName, responseHandler);
	var responseTimeout = setTimeout(function() {
		self.removeListener(eventName, responseHandler);
		callback({success: false, error: "ClientRequestPlayersProfile method timed out"});
	}, 4000);

}

CSGOClient.prototype.toAccountID = function(steamid) {
	if (steamid[0] == "S" || steamid[0] == 's') { // yes
		result = steamid.split(":");
		if (results.length < 3)
			return 0;
		return parseInt(result[1] * 2 + result[2], 10);
	}
	return bignum(steamid).and(0xFFFFFFFF).toNumber();
};

CSGOClient.prototype.toSteamID = function(accountid) {
	return bignum(0x1100001).shiftLeft(32).or(accountid).toString();
};


var handlers = CSGOClient.prototype._handlers = {};

handlers[CSGO.EMsg.ClientWelcome] = function clientWelcomeHandler(message) {
	if (this._gcClientHelloIntervalId) {
		clearInterval(this._gcClientHelloIntervalId);
		this._gcClientHelloIntervalId = null;
		this.ready = true;
	}
};

handlers[CSGO.EMsg.PlayersProfile] = function playersProfileHandler(message) {
	contents = cstrike15_gcmessages.CMsgGCCStrike15_v2_PlayersProfile.parse(message);
	this.emit('players-' + contents.requestId, contents);
}

CSGO.CSGOClient = CSGOClient;
